package com.example.smart_traff.busroute.helper

import android.annotation.SuppressLint
import android.os.Build
import androidx.annotation.RequiresApi
import java.text.SimpleDateFormat
import java.time.LocalDateTime

object TimeFormatHelper {
    lateinit var reFormatArrivalTimeTextAfterReplaceFirst : String
    lateinit var reFormatCurrentTimeText :String
    @RequiresApi(Build.VERSION_CODES.O)
    fun reformatTime(time: String): Int {
        val reFormatArrivalTimeText = time.replaceBefore("T", "")
                                            .replace("T", "")
                                            .replace("+08:00", "")
        val currentTime = LocalDateTime.now()
        reFormatArrivalTimeTextAfterReplaceFirst = if (reFormatArrivalTimeText.startsWith("00:"))  {
            reFormatArrivalTimeText.replaceFirst("00:", "24:")
        } else {
            reFormatArrivalTimeText
        }
        val reFormatCurrentTime = currentTime.toString().replaceBefore("T", "")
                                                        .replace("T", "")
                                                        .replaceAfter(".", "")
                                                        .replace(".", "")

        reFormatCurrentTimeText = if (reFormatCurrentTime.startsWith("00:")) {
            reFormatCurrentTime.replaceFirst("00:", "24:")
        } else {
            reFormatCurrentTime
        }

        val differentTime = ((convertDateToLong(reFormatArrivalTimeTextAfterReplaceFirst) - convertDateToLong(reFormatCurrentTimeText)) / 1000 / 60)
        return differentTime.toInt()
    }

    @SuppressLint("SimpleDateFormat")
    fun convertDateToLong(dateTime: String): Long {
        val df = SimpleDateFormat("HH:mm:ss")
        return df.parse(dateTime).time
    }
}