package com.example.smart_traff.home.view;


import androidx.test.espresso.DataInteraction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import static androidx.test.InstrumentationRegistry.getInstrumentation;
import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.action.ViewActions.*;
import static androidx.test.espresso.assertion.ViewAssertions.*;
import static androidx.test.espresso.matcher.ViewMatchers.*;

import com.example.smart_traff.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class UpdateBusArrivalTimeUITest {

    @Rule
    public ActivityTestRule<HomeActivity> mActivityTestRule = new ActivityTestRule<>(HomeActivity.class);

    @Test
    public void updateBusArrivalTimeUITest() {
        ViewInteraction recyclerView = onView(
allOf(withId(R.id.busRecyclerView),
childAtPosition(
withClassName(is("android.widget.LinearLayout")),
1)));
        recyclerView.perform(actionOnItemAtPosition(0, click()));
        
        ViewInteraction recyclerView2 = onView(
allOf(withId(R.id.busStopListRecyclerView),
childAtPosition(
withClassName(is("android.widget.LinearLayout")),
1)));
        recyclerView2.perform(actionOnItemAtPosition(0, click()));
        
        ViewInteraction appCompatImageButton = onView(
allOf(withId(R.id.updateImageButton),
childAtPosition(
childAtPosition(
withId(R.id.linearLayout2),
0),
2),
isDisplayed()));
        appCompatImageButton.perform(click());
        
        ViewInteraction appCompatImageButton2 = onView(
allOf(withId(R.id.updateImageButton),
childAtPosition(
childAtPosition(
withId(R.id.linearLayout2),
0),
2),
isDisplayed()));
        appCompatImageButton2.perform(click());
        
        pressBack();
        
        ViewInteraction recyclerView3 = onView(
allOf(withId(R.id.busStopListRecyclerView),
childAtPosition(
withClassName(is("android.widget.LinearLayout")),
1)));
        recyclerView3.perform(actionOnItemAtPosition(1, click()));
        
        pressBack();
        
        ViewInteraction recyclerView4 = onView(
allOf(withId(R.id.busStopListRecyclerView),
childAtPosition(
withClassName(is("android.widget.LinearLayout")),
1)));
        recyclerView4.perform(actionOnItemAtPosition(2, click()));
        
        pressBack();
        
        ViewInteraction recyclerView5 = onView(
allOf(withId(R.id.busStopListRecyclerView),
childAtPosition(
withClassName(is("android.widget.LinearLayout")),
1)));
        recyclerView5.perform(actionOnItemAtPosition(4, click()));
        
        pressBack();
        
        pressBack();
        
        ViewInteraction recyclerView6 = onView(
allOf(withId(R.id.busRecyclerView),
childAtPosition(
withClassName(is("android.widget.LinearLayout")),
1)));
        recyclerView6.perform(actionOnItemAtPosition(2, click()));
        
        ViewInteraction recyclerView7 = onView(
allOf(withId(R.id.busStopListRecyclerView),
childAtPosition(
withClassName(is("android.widget.LinearLayout")),
1)));
        recyclerView7.perform(actionOnItemAtPosition(0, click()));
        
        pressBack();
        
        pressBack();
        
        ViewInteraction recyclerView8 = onView(
allOf(withId(R.id.busRecyclerView),
childAtPosition(
withClassName(is("android.widget.LinearLayout")),
1)));
        recyclerView8.perform(actionOnItemAtPosition(1, click()));
        
        ViewInteraction recyclerView9 = onView(
allOf(withId(R.id.busStopListRecyclerView),
childAtPosition(
withClassName(is("android.widget.LinearLayout")),
1)));
        recyclerView9.perform(actionOnItemAtPosition(0, click()));
        
        ViewInteraction appCompatImageButton3 = onView(
allOf(withId(R.id.updateImageButton),
childAtPosition(
childAtPosition(
withId(R.id.linearLayout2),
0),
2),
isDisplayed()));
        appCompatImageButton3.perform(click());
        
        pressBack();
        
        ViewInteraction recyclerView10 = onView(
allOf(withId(R.id.busStopListRecyclerView),
childAtPosition(
withClassName(is("android.widget.LinearLayout")),
1)));
        recyclerView10.perform(actionOnItemAtPosition(4, click()));
        
        ViewInteraction appCompatImageButton4 = onView(
allOf(withId(R.id.updateImageButton),
childAtPosition(
childAtPosition(
withId(R.id.linearLayout2),
0),
2),
isDisplayed()));
        appCompatImageButton4.perform(click());
        
        pressBack();
        
        ViewInteraction recyclerView11 = onView(
allOf(withId(R.id.busStopListRecyclerView),
childAtPosition(
withClassName(is("android.widget.LinearLayout")),
1)));
        recyclerView11.perform(actionOnItemAtPosition(12, click()));
        
        ViewInteraction appCompatImageButton5 = onView(
allOf(withId(R.id.updateImageButton),
childAtPosition(
childAtPosition(
withId(R.id.linearLayout2),
0),
2),
isDisplayed()));
        appCompatImageButton5.perform(click());
        
        ViewInteraction appCompatImageButton6 = onView(
allOf(withId(R.id.updateImageButton),
childAtPosition(
childAtPosition(
withId(R.id.linearLayout2),
0),
2),
isDisplayed()));
        appCompatImageButton6.perform(click());
        
        ViewInteraction appCompatImageButton7 = onView(
allOf(withId(R.id.updateImageButton),
childAtPosition(
childAtPosition(
withId(R.id.linearLayout2),
0),
2),
isDisplayed()));
        appCompatImageButton7.perform(click());
        }
    
    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup)parent).getChildAt(position));
            }
        };
    }
    }
