package com.example.smart_traff.detail.view;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import com.example.smart_traff.R;
import com.example.smart_traff.detail.helper.BusDetailFragmentPagerAdapter;
import com.google.android.material.tabs.TabLayout;

public class BusDetailActivity extends AppCompatActivity {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private BusDetailFragmentPagerAdapter busDetailFragmentPagerAdapter;
    private TextView busRouteNumTextView;
    private ImageButton updateImageButton;

    private TabLayout.Tab one;
    private TabLayout.Tab two;
//    private TabLayout.Tab three;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_detail);
        updateImageButton = findViewById(R.id.updateImageButton);
        initViews();
        busRouteNumTextView = findViewById(R.id.busNumberBusDetailTextView);
        showBusRouteNum();
        updateImageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initViews();
                showBusRouteNum();
            }
        });
    }

    private void showBusRouteNum() {
        Bundle bundle = getIntent().getExtras();
        String busNum = bundle.getString("busNum");
        busRouteNumTextView.setText(busNum);
    }

    private void initViews() {
        viewPager= (ViewPager) findViewById(R.id.viewPager);
        busDetailFragmentPagerAdapter = new BusDetailFragmentPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(busDetailFragmentPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabLayout.setupWithViewPager(viewPager);

        one = tabLayout.getTabAt(0);
        two = tabLayout.getTabAt(1);
//        three = tabLayout.getTabAt(2);

        TextView road0 = findViewById(R.id.K04);
        TextView road1 = findViewById(R.id.H1);
        TextView road2 = findViewById(R.id.H11);
        TextView road3 = findViewById(R.id.S1J);
    }
}