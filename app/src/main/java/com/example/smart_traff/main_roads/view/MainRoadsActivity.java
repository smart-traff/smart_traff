package com.example.smart_traff.main_roads.view;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.smart_traff.R;

public class MainRoadsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_roads);
    }
}