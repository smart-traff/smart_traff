package com.example.smart_traff.busroute.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.smart_traff.R;
import com.example.smart_traff.busroute.helper.BusStopAdapter;
import com.example.smart_traff.busroute.model.BusStopRepository;
import com.example.smart_traff.busroute.presenter.BusStopContact;
import com.example.smart_traff.busroute.presenter.BusStopPresenter;
import com.example.smart_traff.busroute.view.model.BusStop;
import com.example.smart_traff.map.MapsActivity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public class BusStopActivity extends AppCompatActivity implements BusStopContact.busArrivalTimeView{

    private TextView busRouteNum, busCompanyNameTextView;
    private BusStopPresenter busStopPresenter;
    private RecyclerView busStopListRecyclerView;
    private ImageButton btnGoogleMap1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bus_stop_list);
        busStopListRecyclerView = findViewById(R.id.busStopListRecyclerView);
        busRouteNum = findViewById(R.id.busNumberTextView);
        busCompanyNameTextView = findViewById(R.id.busCoTextView);
        BusStopRepository busStopRepository = new BusStopRepository();
        busStopPresenter = new BusStopPresenter(this, busStopRepository);
        showBusData();
        btnGoogleMap1 = findViewById(R.id.imageBtnGoogleMap);
        btnGoogleMap1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(BusStopActivity.this, MapsActivity.class);
                startActivity(i);
            }
        });
    }

    private void showBusData() {
        Bundle bundle = getIntent().getExtras();
        String busRouteId = bundle.getString("busRouteId");
        String busNum = bundle.getString("busNum");
        String busCompanyName = bundle.getString("companyName");
        busRouteNum.setText(busNum);
        busCompanyNameTextView.setText(busCompanyName);
        netWorkCall(busRouteId);
    }

    private void netWorkCall(String busRouteId) {
        busStopPresenter.networkCall(busRouteId);
    }

    @Override
    public void onBusArrivalTimeResultSuccess(@Nullable List<BusStop> busStopList) {
            busStopListRecyclerView.hasFixedSize();
            busStopListRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
            linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
            busStopListRecyclerView.setLayoutManager(linearLayoutManager);
            BusStopAdapter busStopAdapter = new BusStopAdapter(busStopList, busStopPresenter);
            busStopListRecyclerView.setAdapter(busStopAdapter);
    }

    @Override
    public void onBusArrivalTimeResultFailure(@NotNull Throwable error) {
            Toast.makeText(this, "error = " + error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBusArrivalTimeOnClickResult(@NotNull Intent intent) {
        intent.putExtra("busNum", busRouteNum.getText());
        intent.putExtra("companyName", busCompanyNameTextView.getText());
        startActivity(intent);
    }
}