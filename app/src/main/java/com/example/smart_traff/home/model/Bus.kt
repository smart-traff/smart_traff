package com.example.smart_traff.home.model

import com.google.gson.annotations.SerializedName

data class Bus(
        @SerializedName("ROUTE_ID")
        val routeID: String,
        @SerializedName("ROUTE_NAMEE")
        val routeNum: String,
        @SerializedName("LOC_START_NAMEC")
        val startPoint: String,
        @SerializedName("LOC_END_NAMEC")
        val endPoint: String,
        @SerializedName("COMPANY_CODE")
        val companyName: String,
        @SerializedName("FULL_FARE")
        val fullFare: String
)