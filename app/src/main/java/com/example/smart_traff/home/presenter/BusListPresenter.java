package com.example.smart_traff.home.presenter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.smart_traff.busroute.view.BusStopActivity;
import com.example.smart_traff.home.helper.BusListAdapter;
import com.example.smart_traff.home.model.Bus;
import com.example.smart_traff.home.model.BusRepository;
import com.example.smart_traff.home.view.HomeActivity;

import java.util.ArrayList;
import java.util.List;

public class BusListPresenter implements BusListContact.busPresenter {

    private BusRepository busRepository;
    public BusListContact.busListView busHomeView;

    public BusListPresenter(BusListContact.busListView busHomeView, BusRepository busRepository) {
        this.busRepository = busRepository;
        this.busHomeView = busHomeView;
    }

    @Override
    public void networkCall() {
        busRepository.loadResponse(this);
    }

    @Override
    public void getBusSuccess(List<Bus> result) {
        busHomeView.onBusListResultSuccess(result);
    }

    @Override
    public void getBusOnClick(String busRouteId, String busNum, String companyName) {
        Intent intent = new Intent().setClassName("com.example.smart_traff","com.example.smart_traff.busroute.view.BusStopActivity");
        Bundle bundle = new Bundle();
        bundle.putString("busRouteId", busRouteId);
        bundle.putString("busNum", busNum);
        bundle.putString("companyName", companyName);
        intent.putExtras(bundle);
        busHomeView.onBusListOnClickResult(intent);
    }

    @Override
    public void getBusFailure(Throwable error) {
        busHomeView.onBusListResultFailure(error);
    }

    public void busRouteFilter(String routeNum, List<Bus> mBusList, BusListAdapter busListAdapter) {
        ArrayList<Bus> filteredList = new ArrayList<>();
        for (Bus bus : mBusList) {
            if (bus.getRouteNum().toLowerCase().contains(routeNum.toLowerCase()))
                filteredList.add(bus);
        }
        busListAdapter.filterBusRoute(filteredList);
    }

//    public void filtering(BusListAdapter busListAdapter, ArrayList<Bus> filteredList) {
//        busListAdapter.filterBusRoute(filteredList);
//    }
}
