package com.example.smart_traff.busroute.presenter

import android.content.Intent
import com.example.smart_traff.busroute.model.BusStopRepository
import com.example.smart_traff.busroute.view.model.BusStop
import retrofit2.Response

class BusStopPresenter(val arrivalView : BusStopContact.busArrivalTimeView, val repository: BusStopRepository) : BusStopContact.busArrivalTimePresenter {

    private val busBusStopView: BusStopContact.busArrivalTimeView = arrivalView

    override fun networkCall(busRouteId: String) {
        repository.loadResponse(this, busRouteId)
    }

    override fun getBusArrivalTimeSuccess(lists: List<BusStop>?) {
        busBusStopView.onBusArrivalTimeResultSuccess(lists)
    }

    override fun getBusArrivalFailure(error: Throwable) {
        busBusStopView.onBusArrivalTimeResultFailure(error)
    }

    override fun getBusOnClick(busRouteId: String, stopAscending: String) {
        val intent = Intent().setClassName("com.example.smart_traff","com.example.smart_traff.detail.view.BusDetailActivity")
        intent.putExtra("busRouteId", busRouteId)
        intent.putExtra("stopAscending", stopAscending)
        busBusStopView.onBusArrivalTimeOnClickResult(intent)
    }
}