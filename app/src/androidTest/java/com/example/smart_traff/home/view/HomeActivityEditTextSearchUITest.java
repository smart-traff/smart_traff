package com.example.smart_traff.home.view;


import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.example.smart_traff.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;
import static androidx.test.espresso.action.ViewActions.pressImeActionButton;
import static androidx.test.espresso.action.ViewActions.replaceText;
import static androidx.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withClassName;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class HomeActivityEditTextSearchUITest {

    @Rule
    public ActivityTestRule<HomeActivity> mActivityTestRule = new ActivityTestRule<>(HomeActivity.class);

    @Test
    public void homeActivityEditTextSeachUITest() {
        ViewInteraction appCompatEditText = onView(
                allOf(withId(R.id.plainTextInput),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText.perform(replaceText("206"), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(
                allOf(withId(R.id.plainTextInput), withText("206"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText2.perform(pressImeActionButton());

        ViewInteraction appCompatEditText3 = onView(
                allOf(withId(R.id.plainTextInput), withText("206"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText3.perform(replaceText("20"));

        ViewInteraction appCompatEditText4 = onView(
                allOf(withId(R.id.plainTextInput), withText("20"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText4.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(
                allOf(withId(R.id.plainTextInput), withText("20"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText5.perform(pressImeActionButton());

        ViewInteraction appCompatEditText6 = onView(
                allOf(withId(R.id.plainTextInput), withText("20"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText6.perform(replaceText("2"));

        ViewInteraction appCompatEditText7 = onView(
                allOf(withId(R.id.plainTextInput), withText("2"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText7.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText8 = onView(
                allOf(withId(R.id.plainTextInput), withText("2"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText8.perform(pressImeActionButton());

        ViewInteraction appCompatEditText9 = onView(
                allOf(withId(R.id.plainTextInput), withText("2"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText9.perform(replaceText("123"));

        ViewInteraction appCompatEditText10 = onView(
                allOf(withId(R.id.plainTextInput), withText("123"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText10.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText11 = onView(
                allOf(withId(R.id.plainTextInput), withText("123"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText11.perform(pressImeActionButton());

        ViewInteraction appCompatEditText12 = onView(
                allOf(withId(R.id.plainTextInput), withText("123"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText12.perform(replaceText("1234"));

        ViewInteraction appCompatEditText13 = onView(
                allOf(withId(R.id.plainTextInput), withText("1234"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText13.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText14 = onView(
                allOf(withId(R.id.plainTextInput), withText("1234"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText14.perform(pressImeActionButton());

        ViewInteraction appCompatEditText15 = onView(
                allOf(withId(R.id.plainTextInput), withText("1234"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText15.perform(replaceText("12"));

        ViewInteraction appCompatEditText16 = onView(
                allOf(withId(R.id.plainTextInput), withText("12"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText16.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText17 = onView(
                allOf(withId(R.id.plainTextInput), withText("12"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText17.perform(pressImeActionButton());

        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.busRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView.perform(actionOnItemAtPosition(0, click()));

        ViewInteraction recyclerView2 = onView(
                allOf(withId(R.id.busStopListRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView2.perform(actionOnItemAtPosition(0, click()));

        pressBack();

        pressBack();

        ViewInteraction appCompatEditText18 = onView(
                allOf(withId(R.id.plainTextInput), withText("12"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText18.perform(replaceText("a12"));

        ViewInteraction appCompatEditText19 = onView(
                allOf(withId(R.id.plainTextInput), withText("a12"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText19.perform(closeSoftKeyboard());

        ViewInteraction recyclerView3 = onView(
                allOf(withId(R.id.busRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView3.perform(actionOnItemAtPosition(0, click()));

        ViewInteraction recyclerView4 = onView(
                allOf(withId(R.id.busStopListRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView4.perform(actionOnItemAtPosition(0, click()));

        pressBack();

        ViewInteraction recyclerView5 = onView(
                allOf(withId(R.id.busStopListRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView5.perform(actionOnItemAtPosition(11, click()));

        pressBack();

        pressBack();

        ViewInteraction appCompatEditText20 = onView(
                allOf(withId(R.id.plainTextInput), withText("a12"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText20.perform(click());

        ViewInteraction appCompatEditText21 = onView(
                allOf(withId(R.id.plainTextInput), withText("a12"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText21.perform(replaceText("E42"));

        ViewInteraction appCompatEditText22 = onView(
                allOf(withId(R.id.plainTextInput), withText("E42"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText22.perform(closeSoftKeyboard());

        ViewInteraction recyclerView6 = onView(
                allOf(withId(R.id.busRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView6.perform(actionOnItemAtPosition(0, click()));

        ViewInteraction recyclerView7 = onView(
                allOf(withId(R.id.busStopListRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView7.perform(actionOnItemAtPosition(0, click()));

        pressBack();

        ViewInteraction recyclerView8 = onView(
                allOf(withId(R.id.busStopListRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView8.perform(actionOnItemAtPosition(13, click()));

        pressBack();

        pressBack();

        ViewInteraction appCompatEditText23 = onView(
                allOf(withId(R.id.plainTextInput), withText("E42"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText23.perform(replaceText("E4"));

        ViewInteraction appCompatEditText24 = onView(
                allOf(withId(R.id.plainTextInput), withText("E4"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText24.perform(closeSoftKeyboard());

        ViewInteraction recyclerView9 = onView(
                allOf(withId(R.id.busRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView9.perform(actionOnItemAtPosition(0, click()));

        pressBack();

        ViewInteraction recyclerView10 = onView(
                allOf(withId(R.id.busRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView10.perform(actionOnItemAtPosition(1, click()));

        pressBack();

        ViewInteraction recyclerView11 = onView(
                allOf(withId(R.id.busRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView11.perform(actionOnItemAtPosition(2, click()));

        ViewInteraction recyclerView12 = onView(
                allOf(withId(R.id.busStopListRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView12.perform(actionOnItemAtPosition(6, click()));

        pressBack();

        ViewInteraction recyclerView13 = onView(
                allOf(withId(R.id.busStopListRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView13.perform(actionOnItemAtPosition(12, click()));

        pressBack();

        pressBack();

        ViewInteraction appCompatEditText25 = onView(
                allOf(withId(R.id.plainTextInput), withText("E4"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText25.perform(replaceText("00"));

        ViewInteraction appCompatEditText26 = onView(
                allOf(withId(R.id.plainTextInput), withText("00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText26.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText27 = onView(
                allOf(withId(R.id.plainTextInput), withText("00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText27.perform(pressImeActionButton());

        ViewInteraction appCompatEditText28 = onView(
                allOf(withId(R.id.plainTextInput), withText("00"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText28.perform(replaceText("0"));

        ViewInteraction appCompatEditText29 = onView(
                allOf(withId(R.id.plainTextInput), withText("0"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText29.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText30 = onView(
                allOf(withId(R.id.plainTextInput), withText("0"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText30.perform(pressImeActionButton());

        ViewInteraction recyclerView14 = onView(
                allOf(withId(R.id.busRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView14.perform(actionOnItemAtPosition(2, click()));

        ViewInteraction recyclerView15 = onView(
                allOf(withId(R.id.busStopListRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView15.perform(actionOnItemAtPosition(4, click()));

        pressBack();

        pressBack();

        ViewInteraction recyclerView16 = onView(
                allOf(withId(R.id.busRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView16.perform(actionOnItemAtPosition(5, click()));

        ViewInteraction recyclerView17 = onView(
                allOf(withId(R.id.busStopListRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView17.perform(actionOnItemAtPosition(7, click()));

        pressBack();

        pressBack();

        ViewInteraction appCompatEditText31 = onView(
                allOf(withId(R.id.plainTextInput), withText("0"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText31.perform(replaceText("am"));

        ViewInteraction appCompatEditText32 = onView(
                allOf(withId(R.id.plainTextInput), withText("am"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText32.perform(closeSoftKeyboard());

        ViewInteraction recyclerView18 = onView(
                allOf(withId(R.id.busRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView18.perform(actionOnItemAtPosition(0, click()));

        pressBack();

        ViewInteraction appCompatEditText33 = onView(
                allOf(withId(R.id.plainTextInput), withText("am"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText33.perform(click());

        ViewInteraction appCompatEditText34 = onView(
                allOf(withId(R.id.plainTextInput), withText("am"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText34.perform(replaceText("E"));

        ViewInteraction appCompatEditText35 = onView(
                allOf(withId(R.id.plainTextInput), withText("E"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText35.perform(closeSoftKeyboard());

        ViewInteraction recyclerView19 = onView(
                allOf(withId(R.id.busRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView19.perform(actionOnItemAtPosition(0, click()));

        ViewInteraction recyclerView20 = onView(
                allOf(withId(R.id.busRecyclerView),
                        childAtPosition(
                                withClassName(is("android.widget.LinearLayout")),
                                1)));
        recyclerView20.perform(actionOnItemAtPosition(0, click()));

        pressBack();

        pressBack();

        ViewInteraction appCompatEditText36 = onView(
                allOf(withId(R.id.plainTextInput), withText("E"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText36.perform(replaceText("E23A"));

        ViewInteraction appCompatEditText37 = onView(
                allOf(withId(R.id.plainTextInput), withText("E23A"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText37.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText38 = onView(
                allOf(withId(R.id.plainTextInput), withText("E23A"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText38.perform(pressImeActionButton());

        ViewInteraction appCompatEditText39 = onView(
                allOf(withId(R.id.plainTextInput), withText("E23A"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText39.perform(replaceText("E23"));

        ViewInteraction appCompatEditText40 = onView(
                allOf(withId(R.id.plainTextInput), withText("E23"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText40.perform(closeSoftKeyboard());

        ViewInteraction appCompatEditText41 = onView(
                allOf(withId(R.id.plainTextInput), withText("E23"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.homeActivity),
                                        0),
                                0),
                        isDisplayed()));
        appCompatEditText41.perform(pressImeActionButton());
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
