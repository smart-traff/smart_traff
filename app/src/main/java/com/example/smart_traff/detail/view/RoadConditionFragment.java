package com.example.smart_traff.detail.view;

import android.graphics.Color;
import android.os.Bundle;
import android.os.StrictMode;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.smart_traff.R;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.xml.parsers.DocumentBuilderFactory;

public class RoadConditionFragment extends Fragment {

//    private TextView tunnel0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        View v = inflater.inflate(R.layout.fragment_road_condition, container, false);

        TextView road0 = v.findViewById(R.id.K04);
        TextView road1 = v.findViewById(R.id.H1);
        TextView road2 = v.findViewById(R.id.H11);
        TextView road3 = v.findViewById(R.id.S1J);
        TextView tunnel0 = v.findViewById(R.id.Tunnel0);
        TextView tunnel1 = v.findViewById(R.id.Tunnel1);
        TextView tunnel2 = v.findViewById(R.id.Tunnel2);

        //get four main road data
        try {
            String url = "https://resource.data.one.gov.hk/td/journeytime.xml";
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            System.out.println("======" + url);
            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                    .parse(new InputSource(new StringReader(response.toString())));
            NodeList errNodes = doc.getElementsByTagName("jtis_journey_list");
            NodeList errNodes0 = doc.getElementsByTagName("jtis_journey_time");

            if (errNodes0.getLength() > 0) {
                Element err = (Element) errNodes.item(0);
                for (int count = errNodes0.getLength(); count > 0; count--) {
                    String location = err.getElementsByTagName("LOCATION_ID").item(count - 1).getTextContent();
                    String destination = err.getElementsByTagName("DESTINATION_ID").item(count - 1).getTextContent();
                    switch (location) {
                        case "K04":
                            if (destination.equals("CH")) {
                                road0.setText("公主道南行近愛民邨 往 紅磡海底隧道(出口) " + err.getElementsByTagName("JOURNEY_DATA").item(count - 1).getTextContent() + " 分鐘");
                            }
                            break;
                        case "H1":
                            if (destination.equals("CH")) {
                                road1.setText("告士打道東行近稅務大樓 往 紅磡海底隧道(出口) " + err.getElementsByTagName("JOURNEY_DATA").item(count - 1).getTextContent() + " 分鐘");
                            }
                            break;
                        case "H11":
                            if (destination.equals("CH")) {
                                road2.setText("東區走廊西行近鯉景灣 往 紅磡海底隧道(出口) " + err.getElementsByTagName("JOURNEY_DATA").item(count - 1).getTextContent() + " 分鐘");
                            }
                            break;
                        case "SJ1":
                            if (destination.equals("LRT")) {
                                road3.setText("大埔公路近沙田馬場 往 獅子山隧道(出口) " + err.getElementsByTagName("JOURNEY_DATA").item(count - 1).getTextContent() + " 分鐘");
                            }
                            break;
                    }
//                String Location = err.getElementsByTagName("LOCATION_ID").item(16).getTextContent();
//                    System.out.println("公主道南行近愛民邨 往 紅磡海底隧道(出口) " + err.getElementsByTagName("JOURNEY_DATA").item(16).getTextContent() + " 分鐘");
//                    System.out.println("告士打道東行近稅務大樓 往 紅磡海底隧道(出口) " + err.getElementsByTagName("JOURNEY_DATA").item(2).getTextContent() + " 分鐘");
//                    System.out.println("東區走廊西行近鯉景灣 往 紅磡海底隧道(出口) " + err.getElementsByTagName("JOURNEY_DATA").item(0).getTextContent() + " 分鐘");
//                    System.out.println("大埔公路近沙田馬場 往 獅子山隧道(出口) " + err.getElementsByTagName("JOURNEY_DATA").item(22).getTextContent() + " 分鐘");
//                System.out.println(err.getElementsByTagName("LOCATION_ID").item(16).getTextContent() + "公主道南行近愛民邨 to " + err.getElementsByTagName("DESTINATION_ID").item(16).getTextContent() + " NEED " + err.getElementsByTagName("JOURNEY_DATA").item(16).getTextContent() + " mins");
//                System.out.println(err.getElementsByTagName("LOCATION_ID").item(2).getTextContent() + "告士打道東行近稅務大樓 to " + err.getElementsByTagName("DESTINATION_ID").item(2).getTextContent() + " NEED " + err.getElementsByTagName("JOURNEY_DATA").item(2).getTextContent() + " mins");
//                System.out.println(err.getElementsByTagName("LOCATION_ID").item(0).getTextContent() + "東區走廊西行近鯉景灣 to " + err.getElementsByTagName("DESTINATION_ID").item(0).getTextContent() + " NEED " + err.getElementsByTagName("JOURNEY_DATA").item(0).getTextContent() + " mins");
//                System.out.println(err.getElementsByTagName("LOCATION_ID").item(22).getTextContent() + "大埔公路近沙田馬場 to " + err.getElementsByTagName("DESTINATION_ID").item(22).getTextContent() + " NEED " + err.getElementsByTagName("JOURNEY_DATA").item(22).getTextContent() + " mins");
                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }

        //get three main tunnel data
        try {
            String url = "https://dashboard.data.gov.hk/api/traffic-speed?format=xml";
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            System.out.println("======" + url);
            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
            //print in String
            // System.out.println(response.toString());
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                    .parse(new InputSource(new StringReader(response.toString())));
            NodeList errNodes = doc.getElementsByTagName("links");
            NodeList errNodes0 = doc.getElementsByTagName("link");

            if (errNodes0.getLength() > 0) {
                Element err = (Element) errNodes.item(0);
//                int[] myNum = {10, 20, 30, 40};
//                String Location = err.getElementsByTagName("LOCATION_ID").item(16).getTextContent();
                tunnel0.setText("西區海底隧道 香港往九龍 " + err.getElementsByTagName("road_saturation_level").item(1).getTextContent() + "\n往香港 " + err.getElementsByTagName("road_saturation_level").item(3).getTextContent());
                System.out.println("HELLO");
                tunnel1.setText("紅磡海底隧道 香港往九龍 " + err.getElementsByTagName("road_saturation_level").item(4).getTextContent() + "\n往香港 " + err.getElementsByTagName("road_saturation_level").item(5).getTextContent());
                tunnel2.setText("東區海底隧道 香港往九龍 " + err.getElementsByTagName("road_saturation_level").item(0).getTextContent() + "\n往香港 " + err.getElementsByTagName("road_saturation_level").item(2).getTextContent());
//                System.out.println(err.getElementsByTagName("LOCATION_ID").item(16).getTextContent() + "公主道南行近愛民邨 to " + err.getElementsByTagName("DESTINATION_ID").item(16).getTextContent() + " NEED " + err.getElementsByTagName("JOURNEY_DATA").item(16).getTextContent() + " mins");
//                System.out.println(err.getElementsByTagName("LOCATION_ID").item(2).getTextContent() + "告士打道東行近稅務大樓 to " + err.getElementsByTagName("DESTINATION_ID").item(2).getTextContent() + " NEED " + err.getElementsByTagName("JOURNEY_DATA").item(2).getTextContent() + " mins");
//                System.out.println(err.getElementsByTagName("LOCATION_ID").item(0).getTextContent() + "東區走廊西行近鯉景灣 to " + err.getElementsByTagName("DESTINATION_ID").item(0).getTextContent() + " NEED " + err.getElementsByTagName("JOURNEY_DATA").item(0).getTextContent() + " mins");
//                System.out.println(err.getElementsByTagName("LOCATION_ID").item(22).getTextContent() + "大埔公路近沙田馬場 to " + err.getElementsByTagName("DESTINATION_ID").item(22).getTextContent() + " NEED " + err.getElementsByTagName("JOURNEY_DATA").item(22).getTextContent() + " mins");
            } else {
                // success
            }
        } catch (Exception e) {
            System.out.println(e);
        }
//        beginXMLParsing();


        return v;
    }
}