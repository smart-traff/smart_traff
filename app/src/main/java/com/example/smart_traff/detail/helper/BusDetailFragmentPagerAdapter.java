package com.example.smart_traff.detail.helper;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import com.example.smart_traff.detail.view.DrivingTimeFragment;
import com.example.smart_traff.busroute.view.ArrivalTimeFragment;
import com.example.smart_traff.detail.view.RoadConditionFragment;


public class BusDetailFragmentPagerAdapter extends FragmentPagerAdapter {

//    private String[] mTitles = new String[]{"到站時間", "車程", "路面情況"};
    private String[] mTitles = new String[]{"到站時間", "路面情況"};


    public BusDetailFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 1) {
            return new RoadConditionFragment();
        }
        return new ArrivalTimeFragment();
    }

//    @Override
//    public Fragment getItem(int position) {
//        if (position == 1) {
//            return new DrivingTimeFragment();
//        } else if (position == 2) {
//            return new RoadConditionFragment();
//        }
//        return new ArrivalTimeFragment();
//    }



    @Override
    public int getCount() {
        return mTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles[position];
    }
}