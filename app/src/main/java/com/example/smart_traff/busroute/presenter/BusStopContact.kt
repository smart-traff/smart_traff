package com.example.smart_traff.busroute.presenter

import android.content.Intent
import com.example.smart_traff.busroute.view.model.BusStop

interface BusStopContact {

    interface busArrivalTimeModel {
       fun loadResponse(presenter: BusStopPresenter, busRoute: String)
    }

    interface busArrivalTimeView {
        fun onBusArrivalTimeResultSuccess(result: List<BusStop>?)
        fun onBusArrivalTimeResultFailure(error: Throwable)
        fun onBusArrivalTimeOnClickResult(intent: Intent)
    }

    interface busArrivalTimePresenter {
        fun networkCall(busRoute: String)
        fun getBusArrivalTimeSuccess(lists: List<BusStop>?)
        fun getBusArrivalFailure(error: Throwable)
        fun getBusOnClick(busRoute: String, stopAscending: String)
    }
}