package com.example.smart_traff.home.view;


import androidx.test.espresso.DataInteraction;
import androidx.test.espresso.ViewInteraction;
import androidx.test.filters.LargeTest;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import static androidx.test.InstrumentationRegistry.getInstrumentation;
import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static androidx.test.espresso.action.ViewActions.*;
import static androidx.test.espresso.assertion.ViewAssertions.*;
import static androidx.test.espresso.matcher.ViewMatchers.*;

import com.example.smart_traff.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class HomeActivityBusItemUITest {

    @Rule
    public ActivityTestRule<HomeActivity> mActivityTestRule = new ActivityTestRule<>(HomeActivity.class);

    @Test
    public void homeActivityBusItemUITest() {
        ViewInteraction textView = onView(
allOf(withId(R.id.busStartPointTextView), withText("竹園邨"),
withParent(withParent(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class))),
isDisplayed()));
        textView.check(matches(withText("竹園邨")));
        
        ViewInteraction textView2 = onView(
allOf(withId(R.id.busNumberTextView), withText("1"),
withParent(withParent(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class))),
isDisplayed()));
        textView2.check(matches(withText("1")));
        
        ViewInteraction textView3 = onView(
allOf(withId(R.id.busEndPointTextView), withText("尖沙咀碼頭"),
withParent(withParent(IsInstanceOf.<View>instanceOf(android.widget.LinearLayout.class))),
isDisplayed()));
        textView3.check(matches(withText("尖沙咀碼頭")));
        }
    }
