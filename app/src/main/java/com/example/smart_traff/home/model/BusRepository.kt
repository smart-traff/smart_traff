package com.example.smart_traff.home.model

import com.example.smart_traff.home.helper.BusApi
import com.example.smart_traff.home.helper.BusApiServices
import com.example.smart_traff.home.presenter.BusListContact
import com.example.smart_traff.home.presenter.BusListPresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BusRepository: BusListContact.busListModel {

    private var apiClient: BusApiServices?= null

    override fun loadResponse(busListPresenter: BusListPresenter) {
        apiClient = BusApi.client.create(BusApiServices::class.java)
        val call = apiClient?.fetchAllBusRoute()
        call?.enqueue(object : Callback<List<Bus>> {
            override fun onFailure(call: Call<List<Bus>>, t: Throwable) {
                busListPresenter.getBusFailure(t)
            }

            override fun onResponse(call: Call<List<Bus>>, response: Response<List<Bus>>) {
                val lists = response.body()
                busListPresenter.getBusSuccess(lists)
            }
        })
    }
}