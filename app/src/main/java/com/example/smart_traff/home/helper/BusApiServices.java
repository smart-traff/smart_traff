package com.example.smart_traff.home.helper;

import com.example.smart_traff.home.model.Bus;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface BusApiServices {
    // Get Api method
    @GET("/route")
    Call<List<Bus>> fetchAllBusRoute();
}
