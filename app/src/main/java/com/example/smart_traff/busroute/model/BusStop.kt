package com.example.smart_traff.busroute.view.model

import com.google.gson.annotations.SerializedName

data class BusStop(
        @SerializedName("ROUTE_ID")
        val routeID: String,
        @SerializedName("STOP_NAMEC")
        val stopName: String,
        @SerializedName("STOP_SEQ")
        val stopAscending: String,
        @SerializedName("ROUTE_SEQ")
        val stopType: String
)
