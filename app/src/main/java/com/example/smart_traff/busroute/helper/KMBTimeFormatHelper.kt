package com.example.smart_traff.busroute.helper

import android.annotation.SuppressLint
import android.os.Build
import androidx.annotation.RequiresApi
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*

object KMBTimeFormatHelper {

    lateinit var reFormatArrivalTime: String
    lateinit var reFormatCurrentTime: String
    @RequiresApi(Build.VERSION_CODES.O)
    fun reformatTime(time: String): Int {
        val currentTime = LocalDateTime.now()
        reFormatArrivalTime = time.substring(11,19)
        reFormatCurrentTime = currentTime.toString().substring(11,19)

        val formatter = SimpleDateFormat("HH:mm:ss", Locale.getDefault())
        formatter.timeZone = TimeZone.getTimeZone("UTC")
        val result = formatter.parse(reFormatArrivalTime)
        val hkTimeStrFormatArrivalTime = result.toString().substring(11,19)

        reFormatArrivalTime = if(hkTimeStrFormatArrivalTime.startsWith("00:")) {
            hkTimeStrFormatArrivalTime.replaceFirst("00:","24:")
        } else {
            hkTimeStrFormatArrivalTime
        }

        val differentTime = ((convertDateToLong(reFormatArrivalTime) - convertDateToLong(reFormatCurrentTime)) / 1000 / 60)
        return if(differentTime.toInt() >= 1) {
            differentTime.toInt()
        } else
            0
        }

    @SuppressLint("SimpleDateFormat")
    fun convertDateToLong(dateTime: String): Long {
        val df = SimpleDateFormat("HH:mm:ss")
        return df.parse(dateTime).time
    }
}