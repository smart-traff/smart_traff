package com.example.smart_traff.busroute.helper

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.smart_traff.R
import com.example.smart_traff.busroute.model.KMBBusArrivalTime
import kotlinx.android.synthetic.main.three_bus_arrival_time_row.view.*

class KMBBusArrivalTimeAdapter(private val KMBBusArrivalTimeList : List<KMBBusArrivalTime>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val busArrivalTimeView = LayoutInflater.from(parent.context).inflate(R.layout.three_bus_arrival_time_row, parent, false)
        return BusArrivalTimeViewHolder(busArrivalTimeView)
    }

    override fun getItemCount(): Int = KMBBusArrivalTimeList.size

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        KMBBusArrivalTimeList[position].let { (holder as BusArrivalTimeViewHolder).bind(it) }
    }

    class BusArrivalTimeViewHolder(busArrivalTimeView: View): RecyclerView.ViewHolder(busArrivalTimeView) {
        @RequiresApi(Build.VERSION_CODES.O)
        fun bind(kmbBusArrivalTime: KMBBusArrivalTime?) {
            itemView.minuteTextView?.text = kmbBusArrivalTime?.time?.let{ KMBTimeFormatHelper.reformatTime(it).toString() }
            itemView.siteNameTextView?.text = kmbBusArrivalTime?.stopping?.variant?.destination
        }
    }
}