package com.example.smart_traff.busroute.helper

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

interface BusRouteApi {
    companion object {
        private var retrofit: Retrofit? = null
        val client: Retrofit
            get() {
                if (retrofit == null) {
                    retrofit = Retrofit.Builder()
                            .addConverterFactory(GsonConverterFactory.create())
                            .baseUrl("http://10.0.2.2:3000")
                            .build()
                }
                return retrofit!!
            }
    }
}