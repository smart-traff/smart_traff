package com.example.smart_traff.home.view;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.smart_traff.R;
import com.example.smart_traff.home.helper.BusListAdapter;
import com.example.smart_traff.home.model.Bus;
import com.example.smart_traff.home.model.BusRepository;
import com.example.smart_traff.home.presenter.BusListContact;
import com.example.smart_traff.home.presenter.BusListPresenter;

import java.util.List;

public class HomeActivity extends AppCompatActivity implements BusListContact.busListView{

    // Create Object
    private BusListPresenter busListPresenter;
    private EditText busNumEditText;
    private RecyclerView busRecyclerView;
    private List<Bus> mBusList;
    private BusListAdapter busListAdapter;
    private BusRepository busRepository;
    private BusListContact.busListView busListView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        // Init Object
        busListView = this;
        busRepository = new BusRepository();
        busListPresenter = new BusListPresenter(busListView, busRepository);
        busRecyclerView  = findViewById(R.id.busRecyclerView);
        busNumEditText = findViewById(R.id.plainTextInput);
        networkCall();

        busNumEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable busRouteNum) {
                busListPresenter.busRouteFilter(busRouteNum.toString(), mBusList, busListAdapter);
            }
        });
    }

    // Presenter call Api
    private void networkCall() {
        busListPresenter.networkCall();
    }

    // Override function from BusListPresenter
    @Override
    public void onBusListResultSuccess(List<Bus> busList) {
        mBusList = busList;
        busRecyclerView.hasFixedSize();
        busRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        busRecyclerView.setLayoutManager(linearLayoutManager);
        busListAdapter = new BusListAdapter(mBusList, busListPresenter);
        busRecyclerView.setAdapter(busListAdapter);
    }

    @Override
    public void onBusListResultFailure(Throwable error) {
        Toast.makeText(this, "error = " + error, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBusListOnClickResult(Intent intent) {
        startActivity(intent);
    }

}