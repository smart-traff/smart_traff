package com.example.smart_traff.busroute.model

import com.example.smart_traff.busroute.helper.BusRouteApi
import com.example.smart_traff.busroute.helper.BusRouteServices
import com.example.smart_traff.busroute.presenter.ArrivalTimeContact
import com.example.smart_traff.busroute.presenter.BusArrivalTimePresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CTBNWFBBusArrivalTimeRepository : ArrivalTimeContact.arrivalTimeModel{

    val apiClient = BusRouteApi.client.create(BusRouteServices::class.java)
    override fun loadSPResponse(busArrivalTimePresenter: BusArrivalTimePresenter, busRouteIdStr: String, stopAscendingStr: String) {
        val busRouteId = busRouteIdStr.toInt()
        val stopAscending = stopAscendingStr.toInt()
        val call = apiClient?.fetchAllCTBNWFBSPArrivalTime(busRouteId, stopAscending)
        call?.enqueue(object : Callback<List<CTBNWFBBusArrivalTime>> {
            override fun onFailure(call: Call<List<CTBNWFBBusArrivalTime>>, t: Throwable) {
                busArrivalTimePresenter.getBusSPArrivalTimeFailure(t)
                println("CTBNWFB loadSPResponse = ${t}")
            }

            override fun onResponse(call: Call<List<CTBNWFBBusArrivalTime>>, response: Response<List<CTBNWFBBusArrivalTime>>) {
                busArrivalTimePresenter.getBusSPArrivalTimeSuccess(response.body())
                println("CTBNWFB loadSPResponse = ${response.body()}")
            }
        })
    }

    override fun loadDPResponse(busArrivalTimePresenter: BusArrivalTimePresenter, busRouteIdStr: String, stopAscendingStr: String) {
        val busRouteId = busRouteIdStr.toInt()
        val stopAscending = stopAscendingStr.toInt()
        val call = apiClient?.fetchAllCTBNWFBDPArrivalTime(routeId = busRouteId, stopSEQ = stopAscending)
        call?.enqueue(object : Callback<List<CTBNWFBBusArrivalTime>> {
            override fun onFailure(call: Call<List<CTBNWFBBusArrivalTime>>, t: Throwable) {
                println("CTBNWFB loadDPResponse = ${t}")
                busArrivalTimePresenter.getBusDPArrivalTimeFailure(t)
            }

            override fun onResponse(call: Call<List<CTBNWFBBusArrivalTime>>, response: Response<List<CTBNWFBBusArrivalTime>>) {
                println("CTBNWFB loadDPResponse = ${response.body()}")
                busArrivalTimePresenter.getBusDPArrivalTimeSuccess(response.body())
            }
        })
    }
}