package com.example.smart_traff.busroute.model

import com.google.gson.annotations.SerializedName

data class CTBNWFBBusArrivalTime(
        @SerializedName("route")
        val routeID: String,
        @SerializedName("dest_tc")
        val dest_tc: String,
        @SerializedName("eta")
        val arrivalTime: String
)