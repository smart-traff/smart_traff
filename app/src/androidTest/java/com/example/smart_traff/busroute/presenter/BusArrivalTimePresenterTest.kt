package com.example.smart_traff.busroute.presenter

import android.view.View
import com.example.smart_traff.busroute.model.*

import io.mockk.*
import junit.framework.Assert.assertEquals
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.After
import org.junit.Before
import org.junit.Test

class BusArrivalTimePresenterTest {

    lateinit var presenter : BusArrivalTimePresenter
    @RelaxedMockK
    lateinit var mView : ArrivalTimeContact.arrivalTimeView

    @RelaxedMockK
    lateinit var ctbNWFBRepository: CTBNWFBBusArrivalTimeRepository

    @RelaxedMockK
    lateinit var kmbRepository: KMBBusArrivalTimeRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        presenter = BusArrivalTimePresenter(mView, ctbNWFBRepository, kmbRepository)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    // networkCall
    @Test
    fun networkCall_caseCompanyNameIsKMB_caseLoadSPResponse() {
        // Given
        val busRouteId = "1001"
        val stopAscending = "1"
        val companyName = "KMB"
        // When
        presenter.networkCall(busRouteId, stopAscending, companyName)
        // Then
        verify {
            kmbRepository.loadSPResponse(presenter, busRouteId, stopAscending)
        }
    }

    @Test
    fun networkCall_caseCompanyNameIsKMB_caseLoadDPResponse() {
        // Given
        val busRouteId = "1001"
        val stopAscending = "1"
        val companyName = "KMB"
        // When
        presenter.networkCall(busRouteId, stopAscending, companyName)
        // Then
        verify {
            kmbRepository.loadDPResponse(presenter, busRouteId, stopAscending)
        }
    }

    @Test
    fun networkCall_caseCompanyNamIsKMBAndCTB_caseLoadSPResponse() {
        // Given
        val busRouteId = "1003"
        val stopAscending = "1"
        val companyName = "KMB+CTB"
        // When
        presenter.networkCall(busRouteId, stopAscending, companyName)
        // Then
        verify {
            kmbRepository.loadSPResponse(presenter, busRouteId, stopAscending)
        }
    }
    @Test
    fun networkCall_caseCompanyNamIsKMBAndCTB_caseLoadDPResponse() {
        // Given
        val busRouteId = "1003"
        val stopAscending = "1"
        val companyName = "KMB+CTB"
        // When
        presenter.networkCall(busRouteId, stopAscending, companyName)
        // Then
        verify {
            kmbRepository.loadDPResponse(presenter, busRouteId, stopAscending)
        }
    }

    @Test
    fun networkCall_caseCompanyNamIsKMBAndNWFB_caseLoadSPResponse() {
        // Given
        val busRouteId = "1006"
        val stopAscending = "1"
        val companyName = "KMB+NWFB"
        // When
        presenter.networkCall(busRouteId, stopAscending, companyName)
        // Then
        verify {
            kmbRepository.loadSPResponse(presenter, busRouteId, stopAscending)
        }
    }

    @Test
    fun networkCall_caseCompanyNamIsKMBAndNWFB_caseLoadDPResponse() {
        // Given
        val busRouteId = "1006"
        val stopAscending = "1"
        val companyName = "KMB+NWFB"
        // When
        presenter.networkCall(busRouteId, stopAscending, companyName)
        // Then
        verify {
            kmbRepository.loadDPResponse(presenter, busRouteId, stopAscending)
        }
    }


    @Test
    fun networkCall_caseCompanyNamIsCTB_caseLoadSPResponse() {
        // Given
        val busRouteId = "1480"
        val stopAscending = "1"
        val companyName = "CTB"
        // When
        presenter.networkCall(busRouteId, stopAscending, companyName)
        // Then
        verify {
            ctbNWFBRepository.loadSPResponse(presenter, busRouteId, stopAscending)
        }
    }

    @Test
    fun networkCall_caseCompanyNamIsCTB_caseLoadDPResponse() {
        // Given
        val busRouteId = "1480"
        val stopAscending = "1"
        val companyName = "CTB"
        // When
        presenter.networkCall(busRouteId, stopAscending, companyName)
        // Then
        verify {
            ctbNWFBRepository.loadDPResponse(presenter, busRouteId, stopAscending)
        }
    }


    @Test
    fun networkCall_caseCompanyNamIsNWFB_caseLoadSPResponse() {
        // Given
        val busRouteId = "1475"
        val stopAscending = "1"
        val companyName = "CTB"
        // When
        presenter.networkCall(busRouteId, stopAscending, companyName)
        // Then
        verify {
            ctbNWFBRepository.loadSPResponse(presenter, busRouteId, stopAscending)
        }
    }

    @Test
    fun networkCall_caseCompanyNamIsNWFB_caseLoadDPResponse() {
        // Given
        val busRouteId = "1475"
        val stopAscending = "1"
        val companyName = "CTB"
        // When
        presenter.networkCall(busRouteId, stopAscending, companyName)
        // Then
        verify {
            ctbNWFBRepository.loadDPResponse(presenter, busRouteId, stopAscending)
        }
    }

    // getBusSPArrivalTimeSuccess
    @Test
    fun testGetBusSPArrivalTimeSuccess_caseResultIsSuccess_case_ArrivalTimeNotNull() {
        // Given
        val lists = listOf(CTBNWFBBusArrivalTime("23","北角碼頭","2020-12-17T12:00:00+08:00"))
        // When
        presenter.getBusSPArrivalTimeSuccess(lists)
        // Then
        verify {
            mView.onBusSPArrivalTimeResultSuccess(lists)
        }
    }

    @Test
    fun testGetBusSPArrivalTimeSuccess_caseResultIsEmpty() {
        // Given
        val mList = listOf<CTBNWFBBusArrivalTime>()
        // When
        presenter.getBusSPArrivalTimeSuccess(mList)
        // Then
        verify {
            mView.onBusSPArrivalTimeResultIsEmpty("暫沒到站時間數據", View.VISIBLE)
        }
    }

    // getBusDPArrivalTimeSuccess
    @Test
    fun testGetBusDPArrivalTimeSuccess_caseResultIsSuccess_case_ArrivalTimeNotNull() {
        // Given
        val lists = listOf(CTBNWFBBusArrivalTime("23","蒲飛路","2020-12-17T15:17:43+08:00"))
        // When
        presenter.getBusDPArrivalTimeSuccess(lists)
        // Then
        verify {
            mView.onBusDPArrivalTimeResultSuccess(lists)
        }
    }
    @Test
    fun testGetBusDPArrivalTimeSuccess_caseResultIsEmpty() {
        // Given
        val mList = listOf<CTBNWFBBusArrivalTime>()
        // When
        presenter.getBusDPArrivalTimeSuccess(mList)
        // Then
        verify {
            mView.onBusDPArrivalTimeResultIsEmpty("暫沒到站時間數據", View.VISIBLE)
        }
    }
    // getKMBBusSPArrivalTimeSuccess
    @Test
    fun testGetKMBBusSPArrivalTimeSuccess_caseResultIsSuccess_case_ArrivalTimeNotNull() {
        // Given
        val mList = listOf(KMBBusArrivalTime(Stop(Destination("Star Ferry"), "5.8"), "2020-01-10T08:01:00.540Z"))
        val captureSectionFare = slot<String>()
        // When
        presenter.getKMBBusSPArrivalTimeSuccess(mList)
        // Then
        verify {
            mView.onKMBBusSPArrivalTimeResultSuccess(mList, capture(captureSectionFare), View.VISIBLE)
        }
        assertEquals(captureSectionFare.captured, "5.8")
    }

    @Test
    fun testGetKMBBusSPArrivalTimeSuccess_caseResultIsEmpty() {
        // Given
        val mList = listOf<KMBBusArrivalTime>()
        // When
        presenter.getKMBBusSPArrivalTimeSuccess(mList)
        // Then
        verify {
            mView.onKMBBusSPArrivalTimeResultIsEmpty("暫沒到站時間數據", View.VISIBLE)
        }
    }

    // getKMBBusDPArrivalTimeSuccess
    @Test
    fun testGetKMBBusDPArrivalTimeSuccess_caseResultIsSuccess_case_ArrivalTimeNotNull() {
        // Given
        val mList = listOf(KMBBusArrivalTime(Stop(Destination("Chuk Yuen Estate"), "5.8"), "2020-01-10T08:10:00.540Z"))
        val captureSectionFare = slot<String>()
        // When
        presenter.getKMBBusDPArrivalTimeSuccess(mList)
        // Then
        verify {
            mView.onKMBBusDPArrivalTimeResultSuccess(mList, capture(captureSectionFare), View.VISIBLE)
        }
        assertEquals(captureSectionFare.captured, "5.8")
    }

    @Test
    fun testGetKMBBusDPArrivalTimeSuccess_caseResultIsEmpty() {
        // Given
        val mList = listOf<KMBBusArrivalTime>()
        // When
        presenter.getKMBBusDPArrivalTimeSuccess(mList)
        // Then
        verify {
            mView.onKMBBusDPArrivalTimeResultIsEmpty("暫沒到站時間數據", View.VISIBLE)
        }
    }

    // getBusSPArrivalTimeFailure
    @Test
    fun testGetBusSPArrivalTimeFailure() {
        // Given
        val error = Throwable("cannot connect server",Throwable().cause)
        val captureThrowableError = slot<Throwable>()
        // When
        presenter.getBusSPArrivalTimeFailure(error)
        // Then
        verify {
            mView.onBusSPArrivalTimeResultFailure(capture(captureThrowableError))
        }
        assertEquals(captureThrowableError.captured.message, "cannot connect server")
    }

    // getBusDPArrivalTimeFailure
    @Test
    fun testGetBusDPArrivalTimeFailure() {
        // Given
        val error = Throwable("cannot connect server",Throwable().cause)
        val captureThrowableError = slot<Throwable>()
        // When
        presenter.getBusDPArrivalTimeFailure(error)
        // Then
        verify {
            mView.onBusDPArrivalTimeResultFailure(capture(captureThrowableError))
        }
        assertEquals(captureThrowableError.captured.message, "cannot connect server")
    }

    // getBusKMBSPArrivalTimeFailure
    @Test
    fun testGetKMBBusSPArrivalTimeFailure() {
        // Given
        val error = Throwable("cannot connect server",Throwable().cause)
        val captureThrowableError = slot<Throwable>()
        // When
        presenter.getKMBBusSPArrivalTimeFailure(error)
        // Then
        verify {
            mView.onKMBBusSPArrivalTimeResultFailure(capture(captureThrowableError))
        }
        assertEquals(captureThrowableError.captured.message, "cannot connect server")
    }

    // getBusKMBDPArrivalTimeFailure
    @Test
    fun testGetKMBBusDPArrivalTimeFailure() {
        // Given
        val error = Throwable("cannot connect server",Throwable().cause)
        val captureThrowableError = slot<Throwable>()
        // When
        presenter.getKMBBusDPArrivalTimeFailure(error)
        // Then
        verify {
            mView.onKMBBusDPArrivalTimeResultFailure(capture(captureThrowableError))
        }
        assertEquals(captureThrowableError.captured.message, "cannot connect server")
    }
}