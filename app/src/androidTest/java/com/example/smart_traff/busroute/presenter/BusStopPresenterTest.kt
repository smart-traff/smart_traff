package com.example.smart_traff.busroute.presenter

import android.content.Intent
import com.example.smart_traff.busroute.model.BusStopRepository
import com.example.smart_traff.busroute.view.model.BusStop
import junit.framework.Assert.assertEquals
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.slot
import io.mockk.verify
import org.junit.After
import org.junit.Before
import org.junit.Test

class BusStopPresenterTest {

    lateinit var presenter: BusStopPresenter

    @RelaxedMockK
    lateinit var mView : BusStopContact.busArrivalTimeView

    @RelaxedMockK
    lateinit var busStopRepository : BusStopRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        presenter = BusStopPresenter(mView, busStopRepository)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    // networkCall
    @Test
    fun testNetworkCall() {
        // Given
        val busRouteId = "1001"
        val captureBusRouteId = slot<String>()
        // When
        presenter.networkCall(busRouteId)
        // Then
        verify {
            busStopRepository.loadResponse(presenter, capture(captureBusRouteId))
        }
        assertEquals(captureBusRouteId.captured, "1001")
    }

    @Test
    fun testGetBusArriveTimeSuccess_caseBusStopTypeIsOne() {
        // Given
        val lists = listOf(BusStop("1001","竹園邨總站","1","1"))
        val captureBusStopList = slot<List<BusStop>>()
        // When
        presenter.getBusArrivalTimeSuccess(lists)
        // Then
        verify {
            mView.onBusArrivalTimeResultSuccess(capture(captureBusStopList))
        }
        assertEquals(captureBusStopList.captured[0].stopType,"1")
        assertEquals(captureBusStopList.captured[0].stopName,"竹園邨總站")
    }

    @Test
    fun testGetBusArriveTimeSuccess_caseBusStopTypeIsTwo() {
        // Given
        val lists = listOf(BusStop("1001","尖沙咀碼頭","1","2"))
        val captureBusStopList = slot<List<BusStop>>()
        // When
        presenter.getBusArrivalTimeSuccess(lists)
        // Then
        verify {
            mView.onBusArrivalTimeResultSuccess(capture(captureBusStopList))
        }
        assertEquals(captureBusStopList.captured[0].stopType, "2")
        assertEquals(captureBusStopList.captured[0].stopName, "尖沙咀碼頭")
    }

    @Test
    fun testGetBusArrivalFailure() {
        // Given
        val error = Throwable("cannot connect server", Throwable().cause)
        val captureThrowableError = slot<Throwable>()
        // When
        presenter.getBusArrivalFailure(error)
        verify {
            mView.onBusArrivalTimeResultFailure(capture(captureThrowableError))
        }
        assertEquals(captureThrowableError.captured.message, "cannot connect server")
    }

    @Test
    fun testGetBusOnClick() {
        val intent = Intent()
        val captorIntent = slot<Intent>()
        val busRouteId = "1001"
        val stopAscending = "1"
        intent.putExtra("busRouteId", busRouteId)
        intent.putExtra("stopAscending", stopAscending)
        // When
        presenter.getBusOnClick(busRouteId, stopAscending)
        // Then
        verify {
            mView.onBusArrivalTimeOnClickResult(capture(captorIntent))
        }
        println("captorIntent ${captorIntent.captured.extras.toString()}")
        println("captorIntent ${captorIntent.captured}")
        assertEquals(captorIntent.captured.extras.toString(),"Bundle[{stopAscending=1, busRouteId=1001}]")
        assertEquals(captorIntent.captured.toString(),"Intent { cmp=com.example.smart_traff/.detail.view.BusDetailActivity (has extras) }")
    }
}