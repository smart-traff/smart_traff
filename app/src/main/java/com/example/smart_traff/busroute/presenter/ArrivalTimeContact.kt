package com.example.smart_traff.busroute.presenter

import com.example.smart_traff.busroute.model.CTBNWFBBusArrivalTime
import com.example.smart_traff.busroute.model.KMBBusArrivalTime

interface ArrivalTimeContact {
    interface arrivalTimeModel {
        fun loadSPResponse(presenter: BusArrivalTimePresenter, busRoute: String, stopAscending: String)
        fun loadDPResponse(presenter: BusArrivalTimePresenter, busRoute: String, stopAscending: String)
    }
    interface arrivalTimeView {
        fun onBusSPArrivalTimeResultSuccess(result: List<CTBNWFBBusArrivalTime>?)
        fun onBusDPArrivalTimeResultSuccess(result: List<CTBNWFBBusArrivalTime>?)
        fun onBusSPArrivalTimeResultFailure(error: Throwable)
        fun onBusDPArrivalTimeResultFailure(error: Throwable)
        fun onBusSPArrivalTimeResultIsEmpty(string: String, errorLayoutVisible: Int)
        fun onBusDPArrivalTimeResultIsEmpty(string: String, errorLayoutVisible: Int)

        fun onKMBBusSPArrivalTimeResultSuccess(result: List<KMBBusArrivalTime>?, fare: String, fareLayoutVisible: Int)
        fun onKMBBusDPArrivalTimeResultSuccess(result: List<KMBBusArrivalTime>?, fare: String, fareLayoutVisible: Int)
        fun onKMBBusSPArrivalTimeResultFailure(error: Throwable)
        fun onKMBBusDPArrivalTimeResultFailure(error: Throwable)
        fun onKMBBusSPArrivalTimeResultIsEmpty(string: String, errorLayoutVisible: Int)
        fun onKMBBusDPArrivalTimeResultIsEmpty(string: String, errorLayoutVisible: Int)
    }

    interface busArrivalTimePresenter {
        fun networkCall(busRoute: String, stopAscending: String, companyCode: String)
        fun getBusSPArrivalTimeSuccess(lists: List<CTBNWFBBusArrivalTime>?)
        fun getBusSPArrivalTimeFailure(error: Throwable)
        fun getBusDPArrivalTimeSuccess(lists: List<CTBNWFBBusArrivalTime>?)
        fun getBusDPArrivalTimeFailure(error: Throwable)

        fun getKMBBusSPArrivalTimeSuccess(lists: List<KMBBusArrivalTime>?)
        fun getKMBBusSPArrivalTimeFailure(error: Throwable)
        fun getKMBBusDPArrivalTimeSuccess(lists: List<KMBBusArrivalTime>?)
        fun getKMBBusDPArrivalTimeFailure(error: Throwable)
    }
}