package com.example.smart_traff.busroute.helper

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.smart_traff.R
import com.example.smart_traff.busroute.presenter.BusStopContact
import com.example.smart_traff.busroute.view.model.BusStop
import kotlinx.android.synthetic.main.arrival_time.view.*

class BusStopAdapter(private val busStopList: List<BusStop>, private val busStopPresenter: BusStopContact.busArrivalTimePresenter) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val arrivalTimeView = LayoutInflater.from(parent.context).inflate(R.layout.arrival_time, parent, false)
        return ArrivalTimeViewHolder(arrivalTimeView, busStopPresenter)
    }

    override fun getItemCount(): Int = busStopList.size

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val sortedList = busStopList.sortedWith(compareBy({ it.stopType }, { it.stopAscending.toInt() }))[position]
        sortedList.let { (holder as ArrivalTimeViewHolder).bind(it) }
    }

    class ArrivalTimeViewHolder(arrivalTimeView: View, private val busStopPresenter: BusStopContact.busArrivalTimePresenter) : RecyclerView.ViewHolder(arrivalTimeView) {
        @RequiresApi(Build.VERSION_CODES.O)
        fun bind(busStop: BusStop) {
            itemView.siteNameTextView.text = busStop.stopName
            itemView.stopIDTextView.text = busStop.stopAscending
            if (busStop.stopType == "1") {
                itemView.stopTypeTextView.text = "start point"
            } else {
                itemView.stopTypeTextView.text = "end point"
            }
            itemView.setOnClickListener {
                busStopPresenter.getBusOnClick(busStop.routeID, busStop.stopAscending)
            }
        }
    }
}
