package com.example.smart_traff

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.rule.ActivityTestRule
import com.example.smart_traff.home.view.HomeActivity
import org.junit.Rule
import org.junit.Test

class HomeInstrumentedTest {
    @Rule
    @JvmField
    var rule: ActivityTestRule<HomeActivity> = ActivityTestRule(HomeActivity::class.java)

    @Test
    fun user_can_enter_bus_number(){
        Espresso.onView(ViewMatchers.withId(R.id.plainTextInput)).perform(ViewActions.typeText("281M"))
    }
}