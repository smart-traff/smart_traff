package com.example.smart_traff.home.presenter

import android.content.Intent
import android.os.Bundle
import com.example.smart_traff.home.helper.BusListAdapter
import com.example.smart_traff.home.model.Bus
import com.example.smart_traff.home.model.BusRepository
import io.mockk.MockKAnnotations
import io.mockk.clearAllMocks
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.slot
import io.mockk.verify
import junit.framework.Assert.assertEquals
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.*

class BusListPresenterTest {
    lateinit var presenter : BusListPresenter
    @RelaxedMockK
    lateinit var mView : BusListContact.busListView

    @RelaxedMockK
    lateinit var repository: BusRepository


    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        presenter = BusListPresenter(mView, repository)
    }

    @After
    fun tearDown() {
        clearAllMocks()
    }

    // networkCall
    @Test
    fun testNetworkCall() {
        // When
        presenter.networkCall()
        // Then
        verify {
            repository.loadResponse(presenter)
        }
    }

    // getBusSuccess
    @Test
    fun testGetBusSuccess_CaseBusCompanyNameIsKMB() {
        // Given
        val list = listOf(Bus("1001","1","竹園邨","尖沙咀碼頭","KMB","5.8"))
        val captureBusList = slot<List<Bus>>()
        // When
        presenter.getBusSuccess(list)
        verify {
            mView.onBusListResultSuccess(capture(captureBusList))
        }
        assertEquals(captureBusList.captured[0].companyName,"KMB")
    }

    @Test
    fun testGetBusSuccess_CaseBusCompanyNameIsCTB() {
        // Given
        val list = listOf(Bus("1001","1","摩星嶺","跑馬地(上)","CTB","3.7"))
        val captureBusList = slot<List<Bus>>()
        // When
        presenter.getBusSuccess(list)
        verify {
            mView.onBusListResultSuccess(capture(captureBusList))
        }
        assertEquals(captureBusList.captured[0].companyName,"CTB")
    }

    @Test
    fun testGetBusSuccess_CaseBusCompanyNameIsNWFB() {
        // Given
        val list = listOf(Bus("1475","H1","中環 (天星碼頭)","尖沙咀","NWFB","35.2"))
        val captureBusList = slot<List<Bus>>()
        // When
        presenter.getBusSuccess(list)
        verify {
            mView.onBusListResultSuccess(capture(captureBusList))
        }
        assertEquals(captureBusList.captured[0].companyName,"NWFB")
    }

    @Test
    fun testGetBusSuccess_CaseBusCompanyNameIsKMBAndCTB() {
        // Given
        val list = listOf(Bus("1006","102P","筲箕灣","美孚","KMB+CTB","10.4"))
        val captureBusList = slot<List<Bus>>()
        // When
        presenter.getBusSuccess(list)
        verify {
            mView.onBusListResultSuccess(capture(captureBusList))
        }
        assertEquals(captureBusList.captured[0].companyName,"KMB+CTB")
    }

    @Test
    fun testGetBusSuccess_CaseBusCompanyNameIsKMBAndNWFB() {
        // Given
        val list = listOf(Bus("1010","106","黃大仙","小西灣(藍灣半島)","KMB+NWFB","10.4"))
        val captureBusList = slot<List<Bus>>()
        // When
        presenter.getBusSuccess(list)
        verify {
            mView.onBusListResultSuccess(capture(captureBusList))
        }
        assertEquals(captureBusList.captured[0].companyName,"KMB+NWFB")
    }

    // getBusOnClick
    @Test
    fun testBusOnClick(){
        // Given
        val captorIntent = slot<Intent>()
        val busRouteId = "1001"
        val busNum = "1"
        val companyName= "KMB"
        val bundle = Bundle()
        bundle.putString("busRouteId", busRouteId)
        bundle.putString("busNum", busNum)
        bundle.putString("companyName", companyName)
        // When
        presenter.getBusOnClick(busRouteId, busNum, companyName)
        // Then
        verify {
            mView.onBusListOnClickResult(capture(captorIntent))
        }
        assertEquals(captorIntent.captured.extras.toString(),"Bundle[{busRouteId=1001, busNum=1, companyName=KMB}]")
        assertEquals(captorIntent.captured.toString(),"Intent { cmp=com.example.smart_traff/.busroute.view.BusStopActivity (has extras) }")
    }
    // getBusFailure
    @Test
    fun testGetBusFailure() {
        // Given
        val error = Throwable("cannot connect server",Throwable().cause)
        val captureError = slot<Throwable>()
        // When
        presenter.getBusFailure(error)
        verify {
            mView.onBusListResultFailure(capture(captureError))
        }
        assertEquals(captureError.captured.message, "cannot connect server")
    }
    // busRouteFilter
    @Test
    fun testGetRouteFilter_caseSuccess() {
        val inputRouteNum = "102P"
        val filteredList = ArrayList<Bus>()
        val defaultList = listOf(Bus("1001", "1", "竹園邨", "尖沙咀碼頭", "KMB", "5.8"),
                                 Bus("1001", "1", "摩星嶺", "跑馬地(上)", "CTB", "3.7"),
                                 Bus("1475", "H1", "中環 (天星碼頭)", "尖沙咀", "NWFB", "35.2"),
                                 Bus("1006", "102P", "筲箕灣", "美孚", "KMB+CTB", "10.4"),
                                 Bus("1010", "106", "黃大仙", "小西灣(藍灣半島)", "KMB+NWFB", "10.4"))
        val busListAdapter = BusListAdapter(defaultList,presenter)
        for (bus in defaultList) {
            if (bus.routeNum.toLowerCase().contains(inputRouteNum.toLowerCase())) filteredList.add(bus)
        }
        presenter.busRouteFilter(inputRouteNum,defaultList, busListAdapter)
        assertEquals(filteredList.toString(),"[Bus(routeID=1006, routeNum=102P, startPoint=筲箕灣, endPoint=美孚, companyName=KMB+CTB, fullFare=10.4)]")
    }

    @Test
    fun testGetRouteFilter_caseNotChange() {
        val inputRouteNum = ""
        val filteredList = ArrayList<Bus>()
        val defaultList = listOf(Bus("1001", "1", "竹園邨", "尖沙咀碼頭", "KMB", "5.8"),
                Bus("1001", "1", "摩星嶺", "跑馬地(上)", "CTB", "3.7"),
                Bus("1475", "H1", "中環 (天星碼頭)", "尖沙咀", "NWFB", "35.2"),
                Bus("1006", "102P", "筲箕灣", "美孚", "KMB+CTB", "10.4"),
                Bus("1010", "106", "黃大仙", "小西灣(藍灣半島)", "KMB+NWFB", "10.4"))
        val busListAdapter = BusListAdapter(defaultList,presenter)
        for (bus in defaultList) {
            if (bus.routeNum.toLowerCase().contains(inputRouteNum.toLowerCase())) filteredList.add(bus)
        }
        presenter.busRouteFilter(inputRouteNum,defaultList, busListAdapter)
        assertEquals(filteredList.toString(), defaultList.toString())
    }
}