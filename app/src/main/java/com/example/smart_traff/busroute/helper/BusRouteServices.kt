package com.example.smart_traff.busroute.helper

import com.example.smart_traff.busroute.model.CTBNWFBBusArrivalTime
import com.example.smart_traff.busroute.model.KMBBusArrivalTime
import com.example.smart_traff.busroute.view.model.BusStop
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

public interface BusRouteServices {

    @GET("/rstop/{routeID}")
    fun fetchAllRoute(@Path ("routeID") routeID: Int) : Call<List<BusStop>>
    @GET("/rstop/{routeID}/1/{STOP_SEQ}")
    fun fetchAllCTBNWFBSPArrivalTime(@Path ("routeID") routeId: Int, @Path ("STOP_SEQ") stopSEQ: Int) : Call<List<CTBNWFBBusArrivalTime>>
    @GET("/rstop/{routeID}/2/{STOP_SEQ}")
    fun fetchAllCTBNWFBDPArrivalTime(@Path ("routeID") routeId: Int, @Path ("STOP_SEQ") stopSEQ: Int) : Call<List<CTBNWFBBusArrivalTime>>
    @GET("/rstop/{routeID}/1/{STOP_SEQ}")
    fun fetchAllKMBSPArrivalTime(@Path ("routeID") routeId: Int, @Path ("STOP_SEQ") stopSEQ: Int) : Call<List<KMBBusArrivalTime>>
    @GET("/rstop/{routeID}/2/{STOP_SEQ}")
    fun fetchAllKMBDPArrivalTime(@Path ("routeID") routeId: Int, @Path ("STOP_SEQ") stopSEQ: Int) : Call<List<KMBBusArrivalTime>>
}