package com.example.smart_traff.home.presenter;

import android.content.Intent;
import com.example.smart_traff.home.model.Bus;
import java.util.List;

public interface BusListContact {

    interface busListModel {
        void loadResponse(BusListPresenter presenter);
    }

    interface busListView {
        void onBusListResultSuccess(List<Bus> result);
        void onBusListResultFailure(Throwable error);
        void onBusListOnClickResult(Intent intent);
    }

    interface busPresenter {
        void networkCall();
        void getBusSuccess(List<Bus> lists);
        void getBusOnClick(String busRouteId, String routeNum, String busCompanyName);
        void getBusFailure(Throwable error);
    }
}
