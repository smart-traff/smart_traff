package com.example.smart_traff.busroute.helper

import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.example.smart_traff.R
import com.example.smart_traff.busroute.model.CTBNWFBBusArrivalTime
import kotlinx.android.synthetic.main.three_bus_arrival_time_row.view.*

class CTBNWFBBusArrivalTimeAdapter(private val CTBNWFBBusArrivalTimeList: List<CTBNWFBBusArrivalTime>): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val busArrivalTimeView = LayoutInflater.from(parent.context).inflate(R.layout.three_bus_arrival_time_row, parent, false)
        return BusArrivalTimeViewHolder(busArrivalTimeView)
    }

    override fun getItemCount(): Int = CTBNWFBBusArrivalTimeList.size


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        CTBNWFBBusArrivalTimeList[position].let { (holder as BusArrivalTimeViewHolder).bind(it) }
    }

    class BusArrivalTimeViewHolder(busArrivalTimeView: View) : RecyclerView.ViewHolder(busArrivalTimeView) {
        @RequiresApi(Build.VERSION_CODES.O)
        fun bind(ctbNwfbBusArrivalTime: CTBNWFBBusArrivalTime?) {
                itemView.minuteTextView?.text = ctbNwfbBusArrivalTime?.arrivalTime?.let { TimeFormatHelper.reformatTime(it).toString() }
                itemView.siteNameTextView?.text = ctbNwfbBusArrivalTime?.dest_tc
        }
    }
}
