package com.example.smart_traff.busroute.model

data class KMBBusArrivalTime (
        val stopping: Stop,
        val time: String
)

data class Stop(
        val variant: Destination,
        val fare: String
)
data class Destination(
        val destination: String
)