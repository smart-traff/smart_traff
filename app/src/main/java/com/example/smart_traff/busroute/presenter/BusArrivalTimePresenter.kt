package com.example.smart_traff.busroute.presenter

import android.view.View
import com.example.smart_traff.busroute.model.CTBNWFBBusArrivalTime
import com.example.smart_traff.busroute.model.CTBNWFBBusArrivalTimeRepository
import com.example.smart_traff.busroute.model.KMBBusArrivalTime
import com.example.smart_traff.busroute.model.KMBBusArrivalTimeRepository

class BusArrivalTimePresenter(val arrivalView: ArrivalTimeContact.arrivalTimeView, val CTBNWFBRepository: CTBNWFBBusArrivalTimeRepository, val KMBRepository: KMBBusArrivalTimeRepository) : ArrivalTimeContact.busArrivalTimePresenter {

    private val busArrivalTimeView: ArrivalTimeContact.arrivalTimeView = arrivalView

    override fun networkCall(busRouteId: String, stopAscending: String, companyName: String) {
        println("companyName =  $companyName")
        if (companyName == "KMB" || companyName == "KMB+CTB" || companyName == "KMB+NWFB") {
            KMBRepository.loadSPResponse(this, busRouteId, stopAscending)
            KMBRepository.loadDPResponse(this, busRouteId, stopAscending)
        } else {
            CTBNWFBRepository.loadSPResponse(this, busRouteId, stopAscending)
            CTBNWFBRepository.loadDPResponse(this, busRouteId, stopAscending)
        }
    }

    override fun getBusSPArrivalTimeSuccess(list: List<CTBNWFBBusArrivalTime>?) {
        if(list?.size != 0 && list?.get(0)?.arrivalTime != null) {
            busArrivalTimeView.onBusSPArrivalTimeResultSuccess(list)
        } else {
            busArrivalTimeView.onBusSPArrivalTimeResultIsEmpty("暫沒到站時間數據", View.VISIBLE)
        }
    }

    override fun getBusSPArrivalTimeFailure(error: Throwable) {
        busArrivalTimeView.onBusSPArrivalTimeResultFailure(error)
    }

    override fun getBusDPArrivalTimeSuccess(list: List<CTBNWFBBusArrivalTime>?) {
        if(list?.size != 0 &&list?.get(0)?.arrivalTime != null) {
            busArrivalTimeView.onBusDPArrivalTimeResultSuccess(list)
        } else {
            busArrivalTimeView.onBusDPArrivalTimeResultIsEmpty("暫沒到站時間數據", View.VISIBLE)
        }
    }

    override fun getBusDPArrivalTimeFailure(error: Throwable) {
        busArrivalTimeView.onBusDPArrivalTimeResultFailure(error)
    }

    override fun getKMBBusSPArrivalTimeSuccess(list: List<KMBBusArrivalTime>?) {
        if(list?.size != 0 && list?.get(0)?.time != null) {
            val fare = list?.get(0).stopping.fare
            busArrivalTimeView.onKMBBusSPArrivalTimeResultSuccess(list, fare, View.VISIBLE)
        } else {
            busArrivalTimeView.onKMBBusSPArrivalTimeResultIsEmpty("暫沒到站時間數據", View.VISIBLE)
        }
    }

    override fun getKMBBusSPArrivalTimeFailure(error: Throwable) {
        busArrivalTimeView.onKMBBusSPArrivalTimeResultFailure(error)
    }

    override fun getKMBBusDPArrivalTimeSuccess(list: List<KMBBusArrivalTime>?) {
        if(list?.size != 0 && list?.get(0)?.time != null) {
            val fare = list?.get(0).stopping.fare
            busArrivalTimeView.onKMBBusDPArrivalTimeResultSuccess(list, fare,  View.VISIBLE)
        } else {
            busArrivalTimeView.onKMBBusDPArrivalTimeResultIsEmpty("暫沒到站時間數據", View.VISIBLE)
        }
    }

    override fun getKMBBusDPArrivalTimeFailure(error: Throwable) {
        busArrivalTimeView.onKMBBusDPArrivalTimeResultFailure(error)
    }
}