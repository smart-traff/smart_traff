package com.example.smart_traff.busroute.model

import com.example.smart_traff.busroute.helper.BusRouteApi
import com.example.smart_traff.busroute.helper.BusRouteServices
import com.example.smart_traff.busroute.presenter.ArrivalTimeContact
import com.example.smart_traff.busroute.presenter.BusArrivalTimePresenter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
class KMBBusArrivalTimeRepository : ArrivalTimeContact.arrivalTimeModel {

    val apiClient = BusRouteApi.client.create(BusRouteServices::class.java)
    override fun loadSPResponse(busArrivalTimePresenter: BusArrivalTimePresenter, busRouteIdStr: String, stopAscendingStr: String) {
        val busRouteId = busRouteIdStr.toInt()
        val stopAscending = stopAscendingStr.toInt()
        val call = apiClient?.fetchAllKMBSPArrivalTime(busRouteId, stopAscending)
        call?.enqueue(object: Callback<List<KMBBusArrivalTime>>{
            override fun onFailure(call: Call<List<KMBBusArrivalTime>>, t: Throwable) {
                println("KMBBus loadSPResponse = ${t}")
                busArrivalTimePresenter.getKMBBusSPArrivalTimeFailure(t)
            }

            override fun onResponse(call: Call<List<KMBBusArrivalTime>>, response: Response<List<KMBBusArrivalTime>>) {
                println("KMBBus loadSPResponse = ${response.body()}")
                busArrivalTimePresenter.getKMBBusSPArrivalTimeSuccess(response.body())
            }
        })

    }

    override fun loadDPResponse(busArrivalTimePresenter: BusArrivalTimePresenter, busRouteIdStr: String, stopAscendingStr: String) {
        val busRouteId = busRouteIdStr.toInt()
        val stopAscending = stopAscendingStr.toInt()
        val call = apiClient?.fetchAllKMBDPArrivalTime(busRouteId, stopAscending)
        call?.enqueue(object : Callback<List<KMBBusArrivalTime>> {
            override fun onFailure(call: Call<List<KMBBusArrivalTime>>, t: Throwable) {
                println("KMBBus loadDPResponse = ${t}")
                busArrivalTimePresenter.getKMBBusDPArrivalTimeFailure(t)
            }

            override fun onResponse(call: Call<List<KMBBusArrivalTime>>, response: Response<List<KMBBusArrivalTime>>) {
                println("KMBBus loadDPResponse = ${response.body()}")
                busArrivalTimePresenter.getKMBBusDPArrivalTimeSuccess(response.body())
            }
        })
    }
}