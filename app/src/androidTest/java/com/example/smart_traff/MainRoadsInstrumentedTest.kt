package com.example.smart_traff

import androidx.test.platform.app.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.example.smart_traff.main_roads.view.MainRoadsActivity
import org.junit.Assert
import org.junit.Rule
import org.junit.Test

class MainRoadsInstrumentedTest {
    @Rule
    @JvmField
    var rule: ActivityTestRule<MainRoadsActivity> = ActivityTestRule(MainRoadsActivity::class.java)
    @Test
    fun openMainRoadsPage() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        Assert.assertEquals("com.example.smart_traff", appContext.packageName)
    }
}