package com.example.smart_traff.home.helper

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.smart_traff.R
import com.example.smart_traff.home.model.Bus
import com.example.smart_traff.home.presenter.BusListContact
import com.example.smart_traff.home.presenter.BusListPresenter
import kotlinx.android.synthetic.main.bus_row.view.*

class BusListAdapter(private var busRouteList: List<Bus>, private val busListPresenter: BusListPresenter): RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val busRouteView = LayoutInflater.from(parent.context).inflate(R.layout.bus_row,parent,false)
        return BusRouteViewHolder(busRouteView, busListPresenter)
    }
    override fun getItemCount(): Int = busRouteList.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        busRouteList[position]?.let { (holder as BusRouteViewHolder).bind(it) }
    }

    fun filterBusRoute(filteredList: List<Bus>) {
        busRouteList = filteredList
        notifyDataSetChanged()
    }

    class BusRouteViewHolder(busRouteItemView: View, private val busListPresenter: BusListContact.busPresenter) : RecyclerView.ViewHolder(busRouteItemView) {
        fun bind(busRoute: Bus) {
            when (busRoute.companyName) {
                "KMB" -> itemView.busCompanyImageView.setImageResource(R.mipmap.kmb_launcher)
                "CTB" -> itemView.busCompanyImageView.setImageResource(R.mipmap.citybus_launcher)
                "NWFB" -> itemView.busCompanyImageView.setImageResource(R.mipmap.firstbus_launcher)
                "KMB+CTB" -> itemView.busCompanyImageView.setImageResource(R.mipmap.kmb_citybus_launcher)
                "KMB+NWFB" -> itemView.busCompanyImageView.setImageResource(R.mipmap.kmb_firstbus_launcher)
            }
             itemView.busNumberTextView.text = busRoute.routeNum
             itemView.busStartPointTextView.text = busRoute.startPoint
             itemView.busEndPointTextView.text = busRoute.endPoint
             itemView.fullFareTextView.text = busRoute.fullFare

             itemView.setOnClickListener {
                 busListPresenter.getBusOnClick(busRoute.routeID, busRoute.routeNum, busRoute.companyName)
             }
         }
    }
}
