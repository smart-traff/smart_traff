package com.example.smart_traff.busroute.view;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.jetbrains.annotations.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.smart_traff.R;
import com.example.smart_traff.busroute.helper.CTBNWFBBusArrivalTimeAdapter;
import com.example.smart_traff.busroute.helper.KMBBusArrivalTimeAdapter;
import com.example.smart_traff.busroute.model.CTBNWFBBusArrivalTime;
import com.example.smart_traff.busroute.model.CTBNWFBBusArrivalTimeRepository;
import com.example.smart_traff.busroute.model.KMBBusArrivalTime;
import com.example.smart_traff.busroute.model.KMBBusArrivalTimeRepository;
import com.example.smart_traff.busroute.presenter.ArrivalTimeContact;
import com.example.smart_traff.busroute.presenter.BusArrivalTimePresenter;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class ArrivalTimeFragment extends Fragment implements ArrivalTimeContact.arrivalTimeView {

    public ArrivalTimeContact.busArrivalTimePresenter busArrivalTimePresenter;
    public CTBNWFBBusArrivalTimeRepository CTBNWFBBusArrivalTimeRepository;
    public KMBBusArrivalTimeRepository KMBBusArrivalTimeRepository;
    public RecyclerView spBusArrivalTimeRecyclerView, dpBusArrivalTimeRecyclerView;
    public LinearLayout spErrorLinearLayout, dpErrorLinearLayout, spFareLayout, dpFareLayout;
    public TextView spEmptyTextView, dpEmptyTextView, spFareTextView, dpFareTextView;
    @SuppressLint("ResourceType")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View arrivalTimeView = inflater.inflate(R.layout.fragment_arrival_time_detail, container, false);
        CTBNWFBBusArrivalTimeRepository = new CTBNWFBBusArrivalTimeRepository();
        KMBBusArrivalTimeRepository = new KMBBusArrivalTimeRepository();
        busArrivalTimePresenter = new BusArrivalTimePresenter( this, CTBNWFBBusArrivalTimeRepository, KMBBusArrivalTimeRepository);
        spBusArrivalTimeRecyclerView = arrivalTimeView.findViewById(R.id.spArrivalTimeDetailRecyclerView);
        dpBusArrivalTimeRecyclerView = arrivalTimeView.findViewById(R.id.dpArrivalTimeDetailRecyclerView);
        spEmptyTextView = arrivalTimeView.findViewById(R.id.emptySPTextView);
        dpEmptyTextView = arrivalTimeView.findViewById(R.id.emptyDPTextView);
        spFareTextView = arrivalTimeView.findViewById(R.id.spFareTextView);
        dpFareTextView = arrivalTimeView.findViewById(R.id.dpFareTextView);
        spErrorLinearLayout = arrivalTimeView.findViewById(R.id.spErrorLinearLayout);
        dpErrorLinearLayout = arrivalTimeView.findViewById(R.id.dpErrorLinearLayout);
        spFareLayout = arrivalTimeView.findViewById(R.id.spFareLayout);
        dpFareLayout = arrivalTimeView.findViewById(R.id.dpFareLayout);
        // default emptySPTextView / emptyDPTextView is Gone
        spErrorLinearLayout.setVisibility(View.GONE);
        dpErrorLinearLayout.setVisibility(View.GONE);
        spFareLayout.setVisibility(View.GONE);
        dpFareLayout.setVisibility(View.GONE);
        networkCall();
        return arrivalTimeView;
    }

    private void networkCall() {
        // Get busNum from HomeActivity
        Bundle bundle = getActivity().getIntent().getExtras();
        String busRouteId = bundle.getString("busRouteId");
        String stopAscending = bundle.getString("stopAscending");
        String companyName = bundle.getString("companyName");
        System.out.println("get companyName from  " + companyName);
        busArrivalTimePresenter.networkCall(busRouteId, stopAscending, companyName);
    }

    @Override
    public void onBusSPArrivalTimeResultSuccess(@NonNull List<CTBNWFBBusArrivalTime> CTBNWFBBusArrivalTimesList) {
        spBusArrivalTimeRecyclerView.hasFixedSize();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        spBusArrivalTimeRecyclerView.setLayoutManager(linearLayoutManager);
        CTBNWFBBusArrivalTimeAdapter CTBNWFBBusArrivalTimeAdapter = new CTBNWFBBusArrivalTimeAdapter(CTBNWFBBusArrivalTimesList);
        spBusArrivalTimeRecyclerView.setAdapter(CTBNWFBBusArrivalTimeAdapter);
    }

    @Override
    public void onBusDPArrivalTimeResultSuccess(@NonNull List<CTBNWFBBusArrivalTime> CTBNWFBBusArrivalTimesList) {
        dpBusArrivalTimeRecyclerView.hasFixedSize();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dpBusArrivalTimeRecyclerView.setLayoutManager(linearLayoutManager);
        CTBNWFBBusArrivalTimeAdapter CTBNWFBBusArrivalTimeAdapter = new CTBNWFBBusArrivalTimeAdapter(CTBNWFBBusArrivalTimesList);
        dpBusArrivalTimeRecyclerView.setAdapter(CTBNWFBBusArrivalTimeAdapter);
    }

    @Override
    public void onBusSPArrivalTimeResultFailure(@NotNull Throwable error) {
        System.out.println("onBusSPArrivalTimeResultFailure error is " + error);
    }

    @Override
    public void onBusDPArrivalTimeResultFailure(@NotNull Throwable error) {
        System.out.println("onBusDPArrivalTimeResultFailure error is " + error);
    }

    @Override
    public void onBusSPArrivalTimeResultIsEmpty(@NotNull String emptyNoteStr, int errorLayoutVisible) {
        spErrorLinearLayout.setVisibility(errorLayoutVisible);
        spEmptyTextView.setText(emptyNoteStr);
    }

    @Override
    public void onBusDPArrivalTimeResultIsEmpty(@NotNull String emptyNoteStr, int errorLayoutVisible) {
        dpErrorLinearLayout.setVisibility(errorLayoutVisible);
        dpEmptyTextView.setText(emptyNoteStr);
    }

    @Override
    public void onKMBBusSPArrivalTimeResultSuccess(@Nullable List<KMBBusArrivalTime>  kmbBusArrivalTimeList, String fare, int fareLayoutVisible) {
        spFareLayout.setVisibility(fareLayoutVisible);
        spFareTextView.setText(fare);
        spBusArrivalTimeRecyclerView.hasFixedSize();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        spBusArrivalTimeRecyclerView.setLayoutManager(linearLayoutManager);
        KMBBusArrivalTimeAdapter kmbBusArrivalTimeAdapter = new KMBBusArrivalTimeAdapter(kmbBusArrivalTimeList);
        spBusArrivalTimeRecyclerView.setAdapter(kmbBusArrivalTimeAdapter);
    }

    @Override
    public void onKMBBusDPArrivalTimeResultSuccess(@Nullable List<KMBBusArrivalTime> kmbBusArrivalTimeList, String fare, int fareLayoutVisible) {
        dpFareLayout.setVisibility(fareLayoutVisible);
        dpFareTextView.setText(fare);
        dpBusArrivalTimeRecyclerView.hasFixedSize();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this.getContext());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        dpBusArrivalTimeRecyclerView.setLayoutManager(linearLayoutManager);
        KMBBusArrivalTimeAdapter kmbBusArrivalTimeAdapter = new KMBBusArrivalTimeAdapter(kmbBusArrivalTimeList);
        dpBusArrivalTimeRecyclerView.setAdapter(kmbBusArrivalTimeAdapter);
    }

    @Override
    public void onKMBBusSPArrivalTimeResultFailure(@NotNull Throwable error) {
        System.out.println("onKMBBusSPArrivalTimeResultFailure error is " + error);
    }

    @Override
    public void onKMBBusDPArrivalTimeResultFailure(@NotNull Throwable error) {
        System.out.println("onKMBBusDPArrivalTimeResultFailure error is " + error);
    }

    @Override
    public void onKMBBusSPArrivalTimeResultIsEmpty(@NotNull String emptyNoteStr, int errorLayoutVisible) {
        spErrorLinearLayout.setVisibility(errorLayoutVisible);
        spEmptyTextView.setText(emptyNoteStr);
    }

    @Override
    public void onKMBBusDPArrivalTimeResultIsEmpty(@NotNull String emptyNoteStr, int errorLayoutVisible) {
        dpErrorLinearLayout.setVisibility(errorLayoutVisible);
        dpEmptyTextView.setText(emptyNoteStr);
    }
}
