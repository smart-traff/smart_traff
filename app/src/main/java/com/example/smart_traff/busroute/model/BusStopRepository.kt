package com.example.smart_traff.busroute.model

import com.example.smart_traff.busroute.helper.BusRouteApi
import com.example.smart_traff.busroute.helper.BusRouteServices
import com.example.smart_traff.busroute.presenter.BusStopContact
import com.example.smart_traff.busroute.presenter.BusStopPresenter
import com.example.smart_traff.busroute.view.model.BusStop
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BusStopRepository: BusStopContact.busArrivalTimeModel {

    private var apiClient: BusRouteServices?= null
    override fun loadResponse(busBusStopPresenter: BusStopPresenter, busRouteIdStr: String) {
        apiClient = BusRouteApi.client.create(BusRouteServices::class.java)
        val busRouteId = busRouteIdStr.toInt()
        val call = apiClient?.fetchAllRoute(routeID = busRouteId)
        call?.enqueue(object : Callback<List<BusStop>> {
            override fun onFailure(call: Call<List<BusStop>>, t: Throwable) {
                busBusStopPresenter.getBusArrivalFailure(t)
            }

            override fun onResponse(call: Call<List<BusStop>>, response: Response<List<BusStop>>) {
                val lists = response.body()
                busBusStopPresenter.getBusArrivalTimeSuccess(lists)
            }
        })
    }
}